import Vue from 'vue'
import Router from 'vue-router'
import Landing from './views/Landing'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Landing
        },
        {
            path: '/main',
            component: () => import('./views/Main.vue')
        },
        {
            path:'/create',
            component: ()=> import('./views/create.vue')
        }
    ]
})