import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router'
import router from './router'
import store from './store'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueNouislider from 'vue-nouislider'
import 'swiper/css/swiper.css'


Vue.use(VueRouter)
Vue.use(VueAwesomeSwiper)
Vue.use(VueNouislider)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
