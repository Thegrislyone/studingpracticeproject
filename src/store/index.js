import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [createPersistedState()],
    actions: {
      addQuiz(ctx, quiz) {
        ctx.commit('addQuiz', quiz)
      }
    },
    mutations: {
      addQuiz(state, quiz) {
        console.log(quiz)
        const id = this.state.quizes[this.state.quizes.length - 1].id + 1
        // console.log(state)
        // console.log(id)
        // console.log(name + " - name, id - " + id)
        quiz.id = id
        state.quizes.push(quiz)
      }
    },
    state: {
        quizes: [
            {
              id:0,
              title: "Квиз1",
              isLastQuestion: false,
              final: false,
              formInformation: {
                number: true,
                email: true
              },
              questions:[
                {
                  id:0,
                  isActive: true,
                  type: 'img',
                  question: 'С какой целью хотите использовать квиз?',
                  user_answer: '',
                  answers: [
                    {
                      id:0,
                      type: 'checkbox',
                      img: "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/apple/237/beach-with-umbrella_1f3d6.png",
                      text: 'Для себя/для своего бизнеса, я предприниматель'
                    },
                    {
                      id:1,
                      type: 'checkbox',
                      img: "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/apple/237/receipt_1f9fe.png",
                      text: 'Для клиента, я маркетолог'
                    },
                  ]
                },
                {
                  id:1,
                  isActive: false,
                  type: 'text',
                  question: 'Чего хотите добиться в конечном итоге при помощи квиза?',
                  user_answer: '',
                  answers:[
                    {
                      id:0,
                      type: 'checkbox',
                      text: 'повысить лояльность аудитории'
                    },
                    {
                      id:1,
                      type: 'checkbox',
                      text: 'нужно как можно больше заявок/лидов/продаж'
                    },
                    {
                      id:2,
                      type: 'checkbox',
                      text: 'квиз - быстрый способ протестировать нишу/гипотезу'
                    },
                    {
                      id:3,
                      type: 'checkbox',
                      text: 'использую квиз как помощь в подборе товара/услуги'
                    },
                    {
                      id:4,
                      type: 'checkbox',
                      text: 'квиз использую как тест'
                    },
                    {
                      id:5,
                      type: 'text',
                    }
                  ]
                },
                {
                  id:2,
                  isActive: false,
                  type: 'text',
                  question: 'Ваш опыт создания квизов',
                  user_answer: '',
                  answers:[
                    {
                      id:0,
                      type: 'checkbox',
                      text: 'впервые пробую'
                    },
                    {
                      id:1,
                      type: 'checkbox',
                      text: '1-3 квиза'
                    },
                    {
                      id:2,
                      type: 'checkbox',
                      text: 'постоянно создаю квизы'
                    },
                  ]
                },
                {
                  id:3,
                  isActive: false,
                  type: 'text',
                  question: 'Что нам стоить улучшить?',
                  user_answer: '',
                  answers:[
                    {
                      id:0,
                      type: 'checkbox',
                      text: 'функционал'
                    },
                    {
                      id:1,
                      type: 'checkbox',
                      text: 'дизайн/красота'
                    },
                    {
                      id:2,
                      type: 'checkbox',
                      text: 'удобство интерфейса'
                    },
                    {
                      id:3,
                      type: 'checkbox',
                      text: 'интеграции'
                    },
                    {
                      id:4,
                      type: 'checkbox',
                      text: 'техническая поддержка'
                    },
                    {
                      id:5,
                      type: 'text'
                    }
                  ]
                },
                {
                  id:4,
                  isActive: false,
                  type: 'text',
                  question: 'Куда хотите поставить квиз?',
                  user_answer: '',
                  answers:[
                    {
                      id:0,
                      type: 'checkbox',
                      text: 'сайт/лендинг'
                    },
                    {
                      id:1,
                      type: 'checkbox',
                      text: 'Instagram'
                    },
                    {
                      id:2,
                      type: 'checkbox',
                      text: 'Вконтакте'
                    },
                    {
                      id:3,
                      type: 'checkbox',
                      text: 'у меня нет ничего, хочу вести трафик прямо на квиз'
                    }
                  ]
                },
                {
                  id:5,
                  isActive: false,
                  type: 'range',
                  question: 'Предполагаемое количество клиентов в день',
                  user_answer: '',
                  max: 140,
                  min: 0,
                  step: 1
                }
              ]
            },
          ]
    },
    getters: {
      allQuizes(state) {
        return state.quizes
      }
    },
    modules: {

    }
})